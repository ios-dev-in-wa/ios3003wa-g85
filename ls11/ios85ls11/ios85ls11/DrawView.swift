//
//  DrawView.swift
//  ios85ls11
//
//  Created by WA on 18.05.2021.
//

import UIKit

class DrawView: UIView {

    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        let bezierPath = UIBezierPath()
        bezierPath.move(to: CGPoint(x: 200, y: 200))
        bezierPath.addLine(to: CGPoint(x: 20, y: 50))
        bezierPath.addLine(to: CGPoint(x: 100, y: 50))
        bezierPath.addLine(to: CGPoint(x: 100, y: 20))
        bezierPath.addLine(to: CGPoint(x: 200, y: 200))
        bezierPath.close()

        bezierPath.lineWidth = 2

        UIColor.blue.setFill()
        UIColor.red.setStroke()

        bezierPath.stroke()
        bezierPath.fill()
    }

}
