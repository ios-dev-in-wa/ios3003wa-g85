//
//  DrawingViewController.swift
//  ios85ls11
//
//  Created by WA on 18.05.2021.
//

import UIKit
import PKHUD
import NVActivityIndicatorView

class DrawingViewController: UIViewController {

    @IBOutlet weak var boxView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        print(Settings.shared.appColor)
        view.backgroundColor = UIColor.hexStringToUIColor(hex: Settings.shared.appColor)
        let nvActivityView = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 100, height: 100), type: .orbit, color: .cyan, padding: 0)
//        view.addSubview(nvActivityView)
//        nvActivityView.center = view.center
//        nvActivityView.startAnimating()
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        let drawView = DrawView(frame: view.frame)
//        view.addSubview(drawView)
    }

    @IBAction func showPickerAction(_ sender: UIButton) {
//        HUD.flash(.label("Hello There"))
//        UIView.animate(withDuration: 1) {
//            self.boxView.backgroundColor = .random
////            self.boxView.frame.origin = CGPoint(x: 0, y: 0)
////            self.boxView.frame = CGRect(x: 10, y: 100, width: 10, height: 100)
////            self.boxView.transform = .init(scaleX: 1.5, y: 1.5)
//            self.boxView.transform = .init(rotationAngle: CGFloat.pi / 2)
////            self.boxView.center
////            self.boxView.alpha
////            self.boxView.contentMode
////            self.boxView.transform = .
//        }
        
//        UIView.animate(withDuration: 1, delay: 0.2, options: [.autoreverse]) {
//            self.boxView.transform = .init(rotationAngle: CGFloat.pi / 2)
//        } completion: { isFinished in
//            self.boxView.transform = .identity
//        }
        UIView.transition(with: boxView, duration: 2, options: [.transitionFlipFromLeft]) {
            self.boxView.backgroundColor = .random
        } completion: { _ in
            
        }

//        UIView.transition(from: boxView, to: boxView, duration: 2, options: [.transitionFlipFromTop], completion: nil)
//        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
//            // Remove animations from view
//            self.boxView.layer.removeAllAnimations()
//        }

//        let colorVC = UIColorPickerViewController()
//        colorVC.delegate = self
//        present(colorVC, animated: true, completion: nil)
    }
}

extension DrawingViewController: UIColorPickerViewControllerDelegate {
    func colorPickerViewControllerDidSelectColor(_ viewController: UIColorPickerViewController) {
//        UIColor(red: <#T##CGFloat#>, green: <#T##CGFloat#>, blue: <#T##CGFloat#>, alpha: <#T##CGFloat#>)
        
        Settings.shared.appColor = viewController.selectedColor.hexString
        view.backgroundColor = viewController.selectedColor
    }

    func colorPickerViewControllerDidFinish(_ viewController: UIColorPickerViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
}


extension UIColor {
    var hexString: String {
        let components = cgColor.components
        let r: CGFloat = components?[0] ?? 0.0
        let g: CGFloat = components?[1] ?? 0.0
        let b: CGFloat = components?[2] ?? 0.0
        
        let hexString = String.init(format: "#%02lX%02lX%02lX", lroundf(Float(r * 255)), lroundf(Float(g * 255)), lroundf(Float(b * 255)))
        print(hexString)
        return hexString
    }

    static func hexStringToUIColor(hex: String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6) {
            return UIColor.gray
        }

        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)

        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}

extension CGFloat {
    static var random: CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}

extension UIColor {
    static var random: UIColor {
        return UIColor(red: .random, green: .random, blue: .random, alpha: 1.0)
    }
}
