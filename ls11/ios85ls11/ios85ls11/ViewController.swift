//
//  ViewController.swift
//  ios85ls11
//
//  Created by WA on 18.05.2021.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        scrollView.delegate = self
    }

    override func viewDidAppear(_ animated: Bool) {
        scrollView.contentInset = view.safeAreaInsets
//        let obj = Settings()
        Settings.shared.hello
    }
}

extension ViewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
}
