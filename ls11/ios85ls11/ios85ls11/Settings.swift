//
//  Settings.swift
//  ios85ls11
//
//  Created by WA on 18.05.2021.
//

import Foundation

// userdefaults
final class Settings {
    static let shared = Settings()

    private init() { }

    let hello = "Hello"

    private let defaults = UserDefaults.standard

    var visitsCount: Int {
        get {
            defaults.integer(forKey: "visitsCount")
        }
        set {
            defaults.set(newValue, forKey: "visitsCount")
        }
    }


    var appColor: String {
        get {
            defaults.string(forKey: "appColor") ?? "#ffe700ff"
        }
        set {
            defaults.set(newValue, forKey: "appColor")
        }
    }

//    func storeValue(value: Any, key: String) {
//        defaults.set(value, forKey: key)
//    }
//
//    func readValue(forKey: String) -> Any? {
//        defaults.value(forKey: forKey)
//    }
}
