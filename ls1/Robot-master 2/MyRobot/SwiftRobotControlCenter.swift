//
//  SwiftRobotControlCenter.swift
//  MyRobot
//
//  Created by Ivan Vasilevich on 10/4/14.
//  Copyright (c) 2014 Ivan Besarab. All rights reserved.
//

import UIKit
//  All robot commands can be founded in GameViewController.h
class SwiftRobotControlCenter: RobotControlCenter {
    
    //  Level name setup
    override func viewDidLoad() {
        levelName = "L3C" //  Level name
        super.viewDidLoad()
    }
    
    override func run() {
        solveFirstTask()
        
    }

    func solveFirstTask() {
        findPeak()
        goToPeak()
        uTurn()
        goDown()
    }
    
    func forwardClear() {
        while frontIsClear {
            move()
        }
    }
    
    // move() - сделать шаг вперед
    // pick() - положить
    // put() - поднять
    // turnRight() - поворот на право
    //frontIsClear - впереди нет препятствия
    //leftIsClear - слева нет препятствия
    //rightIsClear - справа нет препятствия
    //candyPresent - есть мешок под роботом
    //candyInBag - мешок есть в роботе
    //facingUp - смотрит вверх
    //facingDown - смотрит вниз
    //facingRight - смотрит вправо
    //facingLeft - смотрит влево
    
    func findPeak() {
        
    }

    func goToPeak() {
        
    }

    func uTurn() {
        
    }

    func goDown() {
        
    }
    
    // DRY

    func whileExample() {
        while frontIsClear {
            move()
            if candyPresent {
//                pick()
//                turnRight()
                move()
                break
            }
        }
    }

    func forExample() {
        #warning("LOOK AT ME")
        for _ in 0..<4 {
            move()
        }
    }

    /*
     MULTILINE
        COMMENT
            SDASD
     */
    func ifElse() {
        if frontIsClear, noCandyPresent {
            doubleMove()

            if candyPresent {
                pick()
                move()
                turnRight()
                doubleMove()
            } else {
                move()
                pick()
            }
        }
    }
    
    func solvePuzzle() {
        doubleMove()
        move()
        pick()
        turnRight()
        move()
        turnLeft()
        doubleMove()
        put()
        doubleMove()
    }
 
    func doubleMove() {
        move()
        move()
    }
    
    func turnLeft() {
        for _ in 0..<3 {
            turnRight()
        }
        foo()
    }
    
    func foo() {
        
    }
}

