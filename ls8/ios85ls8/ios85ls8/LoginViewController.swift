//
//  LoginViewController.swift
//  ios85ls8
//
//  Created by WA on 27.04.2021.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet var tapGesture: UITapGestureRecognizer!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func tapAction(_ sender: UITapGestureRecognizer) {
        print(sender.location(in: view))
        switch sender.state {
        case .possible:
            print("Possible")
        case .began:
            print("Began")
        case .changed:
            print("Changed")
        case .ended:
            print("Ended")
        case .cancelled:
            print("Cancelled")
        case .failed:
            print("Failed")
        @unknown default:
            print("default")
        }
    }

    var arrayOPoints = [CGPoint]()
    @IBAction func panAction(_ sender: UIPanGestureRecognizer) {
        arrayOPoints.append(sender.location(in: view))
        print(sender.velocity(in: view))
        print(sender.translation(in: view))
        switch sender.state {
        case .possible:
            print("Possible", "Pan")
        case .began:
            print("Began", "Pan")
            print(sender.location(in: view))
        case .changed:
            print(sender.location(in: view))
            print("Changed", "Pan")
        case .ended:
            print("Ended", "Pan")
        case .cancelled:
            print("Cancelled", "Pan")
        case .failed:
            print("Failed", "Pan")
        @unknown default:
            print("default", "Pan")
        }
    }

    @IBAction func pinchAction(_ sender: UIPinchGestureRecognizer) {
        print(sender.scale, sender.velocity)
    }

    @IBAction func rotationAction(_ sender: UIRotationGestureRecognizer) {
        print(sender.rotation, sender.velocity)
    }

    @IBAction func swipeAction(_ sender: UISwipeGestureRecognizer) {
//        sender.
    }
    @IBAction func longPressAction(_ sender: UILongPressGestureRecognizer) {
        
        switch sender.state {
        case .possible:
            print("Possible")
        case .began:
            print("Began")
        case .changed:
            print("Changed")
        case .ended:
            print("Ended")
        case .cancelled:
            print("Cancelled")
        case .failed:
            print("Failed")
        @unknown default:
            print("default")
        }
    }
}
