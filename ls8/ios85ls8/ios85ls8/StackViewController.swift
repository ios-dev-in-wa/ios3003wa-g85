//
//  StackViewController.swift
//  ios85ls8
//
//  Created by WA on 27.04.2021.
//

import UIKit

class StackViewController: UIViewController {

    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var pinkView: UIView!
    @IBOutlet weak var rocketImageView: UIImageView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var chocoLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        stackView.setCustomSpacing(30, after: pinkView)
    }

    @IBAction func saveAction(_ sender: UIButton) {
        chocoLabel.text = textField.text
    }
}
