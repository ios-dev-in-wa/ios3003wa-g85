//
//  MyAlertViewController.swift
//  ios85ls10
//
//  Created by WA on 13.05.2021.
//

import UIKit

class MyAlertViewController: UIViewController {

    let alertView: AlertView = .fromNib()

    override func viewDidLoad() {
        super.viewDidLoad()

        alertView.frame = view.frame
        view.addSubview(alertView)
        alertView.dismissAction = { [weak self] in
            self?.dismiss(animated: true, completion: nil)
        }
    }

    func setupWith(model: AlertModel) {
        alertView.setupWith(model: model)
    }
}
