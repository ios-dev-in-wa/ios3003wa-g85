//
//  ViewController.swift
//  ios85ls10
//
//  Created by WA on 13.05.2021.
//

import UIKit

class StudentClass {
    var name: String

    init(name: String) {
        self.name = name
    }
}

struct StudentStruct {
    var name: String

    init(name: String) {
        self.name = name
    }
}

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
       
    }

    override func viewDidAppear(_ animated: Bool) {
//        writeToFile()
//        readFromFile()
//        saveArray()
//        readArray()
        // VIEW
//        let alertView: AlertView = .fromNib()
//        alertView.setupWith(model: AlertModel(type: .error,
//                                              title: "Error",
//                                              subtitle: "Please try again later",
//                                              buttonAction: {
//                                                print("Button Pressed")
//                                              }))
//        alertView.frame = view.frame
//        view.addSubview(alertView)

        let myAlertViewController = MyAlertViewController()
        myAlertViewController.setupWith(model: AlertModel(type: .error,
                                                          title: "Error",
                                                          subtitle: "Please try again laterPlease try again laterPlease try again laterPlease try again laterPlease try again laterPlease try again laterPlease try again later",
                                                          buttonAction: {
                                                            print("Button Pressed")
                                                      }))
        myAlertViewController.modalTransitionStyle = .crossDissolve
        myAlertViewController.modalPresentationStyle = .overFullScreen
        present(myAlertViewController, animated: true, completion: nil)
    }

    func refValType() {
        // REFERENCE TYPE
        let classStudent = StudentClass(name: "Artem")

        // VALUE TYPE
        var structStudent = StudentStruct(name: "Ihor")
        
        let copyStruct = structStudent
        let copyClass = classStudent

        classStudent.name = "Anna"

        structStudent.name = "Alona"
    }

    func solveBlackBox() {
        let size = CGSize(width: 100, height: 100)
        let minX = size.width / 2
        let maxX = view.frame.width - size.width / 2
        let xPos = CGFloat.random(in: minX...maxX)

        print(view.safeAreaInsets.top)
        print(view.safeAreaInsets.bottom)
        let minY = view.safeAreaInsets.top + size.width / 2
        let maxY = view.frame.height - view.safeAreaInsets.bottom - size.width / 2
        let yPos = CGFloat.random(in: minY...maxY)
    }

    func writeToFile() {
        do {
            let url = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            print(url)
            let stringValue = "Artem Velykyy123123123"

//            Data type is for saving
            try stringValue.data(using: .utf8)?.write(to: url.appendingPathComponent("text.txt"))
        } catch {
            print(error.localizedDescription)
        }
    }

    func readFromFile() {
        do {
            let url = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let fileUrl = url.appendingPathComponent("text.txt")
            let data = try Data(contentsOf: fileUrl)
            print(String(data: data, encoding: .utf8))
        } catch {
            print(error.localizedDescription)
        }
    }

    func saveArray() {
        let arrayOfNames = ["Artem", "Nikita", "Valera"]

        do {
            let encodedData = try JSONEncoder().encode(arrayOfNames)
            let url = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let fileUrl = url.appendingPathComponent("names.txt")
            try encodedData.write(to: fileUrl)
        } catch {
            print(error.localizedDescription)
        }
    }

    func readArray() {
        do {
            let url = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let fileUrl = url.appendingPathComponent("names2.txt")
            let data = try Data(contentsOf: fileUrl)
            print(String(data: data, encoding: .utf8))
//            print([String].self)
            let decodedData = try JSONDecoder().decode([String].self, from: data)
            print(decodedData)
        } catch {
            showAlert(errorString: error.localizedDescription)
            print(error.localizedDescription)
        }
    }

    func showAlertExtended(errorString: String) {
        let alertVC = UIAlertController(title: "Error", message: errorString, preferredStyle: .actionSheet)
        alertVC.addAction(UIAlertAction(title: "Oke", style: .cancel, handler: { _ in
            print("do nothing")
        }))
        alertVC.addAction(UIAlertAction(title: "Go to support", style: .default, handler: { _ in
            print("Call support")
        }))
        alertVC.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { _ in
            print("remove files")
        }))
        present(alertVC, animated: true, completion: nil)
//        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
//            print("test async")
//            alertVC.dismiss(animated: true, completion: nil)
//        }
        print("test sync")
    }
}

extension UIViewController {
    func showAlert(errorString: String) {
        let alertVC = UIAlertController(title: "Error", message: errorString, preferredStyle: .actionSheet)
        alertVC.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
            print("do nothing")
        }))
        present(alertVC, animated: true, completion: nil)
    }
}
