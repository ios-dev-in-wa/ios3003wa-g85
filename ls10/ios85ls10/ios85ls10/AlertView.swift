//
//  AlertView.swift
//  ios85ls10
//
//  Created by WA on 13.05.2021.
//

import UIKit

enum AlertType {
    case success, error

    var imageName: String {
        switch self {
        case .error: return "xmark"
        case .success: return "checkmark"
        }
    }

    var buttonTitle: String {
        switch self {
        case .error: return "Ok"
        case .success: return "Woohooo"
        }
    }

    var color: UIColor {
        switch self {
        case .error: return .red
        case .success: return .green
        }
    }
}

struct AlertModel {
//    let imageName: String
//    let buttonTitle: String
    let type: AlertType
    let title: String
    let subtitle: String
    let buttonAction: (() -> Void)?
}

@IBDesignable
class AlertView: UIView {

    @IBOutlet weak var imageContainerView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var mainButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!

    private var currentModel: AlertModel?
    var dismissAction: (() -> Void)?

    func setupWith(model: AlertModel) {
        currentModel = model
        imageView.image = UIImage(systemName: model.type.imageName)
        mainButton.setTitle(model.type.buttonTitle, for: .normal)
        mainButton.backgroundColor = model.type.color
        imageContainerView.backgroundColor = model.type.color
        titleLabel.text = model.title
        subtitleLabel.text = model.subtitle
    }

    @IBAction func mainButtonAction(_ sender: UIButton) {
        currentModel?.buttonAction()
        // If alertView is presented as VIEW
//        removeFromSuperview()
        dismissAction?()
        
    }
}
