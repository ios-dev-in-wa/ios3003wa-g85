//
//  AddViewController.swift
//  ios85ls9
//
//  Created by WA on 29.04.2021.
//

import UIKit

protocol AddViewControllerDelegate: class {
    func didPressSave(car: Car)
}

class AddViewController: UIViewController {

    @IBOutlet weak var carTitleTextField: UITextField!
    @IBOutlet weak var carPriceTextField: UITextField!
    @IBOutlet weak var imageView: UIImageView!
    
    weak var delegate: AddViewControllerDelegate?

    @IBAction func saveAction(_ sender: UIButton) {
        guard let text = carTitleTextField.text,
              !text.isEmpty,
              let priceText = carPriceTextField.text else { return }
        delegate?.didPressSave(car: Car(title: text, price: Double(priceText) ?? 0.0, currency: "$", image: imageView.image))
        navigationController?.popViewController(animated: true)
    }

    @IBAction func tapAction(_ sender: UITapGestureRecognizer) {
        let picker = UIImagePickerController()
        picker.sourceType = .photoLibrary// .camera
        picker.delegate = self
        present(picker, animated: true, completion: nil)
    }
}

extension AddViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        imageView.image = image
        picker.dismiss(animated: true, completion: nil)
    }
}
