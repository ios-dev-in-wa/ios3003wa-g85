//
//  ViewController.swift
//  ios85ls9
//
//  Created by WA on 29.04.2021.
//

import UIKit

struct Drink {
    let title: String
    let coffeeNeeded: Double
    let waterNeeded: Double
//    ...
}

struct Test {
    var title = ""
}

class TestClass {
    var title: String = ""
}

enum CarCellType {
    case withImage, noImage
}

struct Car {
    let title: String
    let price: Double
    let currency: String
    let image: UIImage?
}

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

//    var autos = [
//        "Mazda",
//        "BMW13245678908765432134567890-098765432134567890-98765432",
//        "Audi",
//        "Ferrari"
//    ]
    var autos = [
        Car(title: "Mazda", price: 10000.0, currency: "$", image: nil),
        Car(title: "BMW", price: 12000.0, currency: "E", image: nil),
        Car(title: "Audi", price: 11000.0, currency: "UAH", image: nil)
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tableView.dataSource = self
        tableView.delegate = self
        
        let testClass = TestClass()
        var testStruct = Test()
        testClass.title = "Artem"

        testStruct.title = "Ihor"

        var someTestStruct = testStruct
        someTestStruct.title = "Alona"
        print(testStruct.title, someTestStruct.title)

        let newTestClass = testClass

        newTestClass.title = "Dima"
        print(newTestClass.title, testClass.title)
    }

    @IBAction func editAction(_ sender: UIBarButtonItem) {
        tableView.setEditing(!tableView.isEditing, animated: true)
        sender.title = tableView.isEditing ? "Done" : "Edit"
    }

    func removeCellAtIndexPath(_ indexPath: IndexPath) {
        autos.remove(at: indexPath.row)
        tableView.deleteRows(at: [indexPath], with: .automatic)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "GoToDetails",
           let vc = segue.destination as? DetailsViewController,
           let indexPath = sender as? IndexPath {

//            guard let index = tableView.indexPathForSelectedRow?.row else { return }
            vc.carTitle = autos[indexPath.row].title
        }
        if segue.identifier == "GoToAddVC",
           let vc = segue.destination as? AddViewController {
            vc.delegate = self
        }
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.row % 2 == 0 ? 60 : 100
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = .red
        return view
    }

    func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: "GoToDetails", sender: indexPath)
    }

    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return "TRASH"
    }

    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let contextAction = UIContextualAction(style: .destructive, title: "Trash") { (_, _, _) in
            print("trash action")
            self.removeCellAtIndexPath(indexPath)
        }
        let swipeConfig = UISwipeActionsConfiguration(actions: [contextAction])
        return swipeConfig
    }

//    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
//        return
//    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return autos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print(indexPath.row, indexPath.section)
        // THIRD VERSION
        let auto = autos[indexPath.row]
        if let image = auto.image {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CarCell", for: indexPath)
            cell.imageView?.image = image
            cell.textLabel?.text = auto.title
            cell.detailTextLabel?.text = "\(auto.price) \(auto.currency)"
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "CustomTableViewCell", for: indexPath) as? CustomTableViewCell else {
                fatalError()
            }
            cell.mainLabel.text = auto.title
            cell.secondaryLabel.text = "\(auto.price) \(auto.currency)"
            return cell
        }
//        switch auto.cellType {
//        case .noImage:
//            guard let cell = tableView.dequeueReusableCell(withIdentifier: "CustomTableViewCell", for: indexPath) as? CustomTableViewCell else {
//                fatalError()
//            }
//            cell.mainLabel.text = auto.title
//            cell.secondaryLabel.text = "\(auto.price) \(auto.currency)"
//            return cell
//        case .withImage:
//            let cell = tableView.dequeueReusableCell(withIdentifier: "CarCell", for: indexPath)
//            cell.imageView?.image = UIImage(systemName: "car")
//            cell.textLabel?.text = auto.title
//            cell.detailTextLabel?.text = "\(auto.price) \(auto.currency)"
//            return cell
//        }
        
        // SECOND VERSION
//        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CustomTableViewCell", for: indexPath) as? CustomTableViewCell else {
//            fatalError()
//        }
//        let auto = autos[indexPath.row]
//        cell.mainLabel.text = auto.title
//        cell.secondaryLabel.text = "\(auto.price) \(auto.currency)"
        
        // FIRST VERSION
//        let cell = tableView.dequeueReusableCell(withIdentifier: "CarCell", for: indexPath)
//        cell.textLabel?.numberOfLines = 0
//        cell.textLabel?.text = autos[indexPath.row]//"\(indexPath.row) ROW"
//        cell.detailTextLabel?.text = "\(indexPath.section) SECTION"
//        return cell
    }

//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "\(section) Section"
    }

    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return "End of \(section) section"
    }

    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return indexPath.row % 2 == 0
    }

    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return indexPath.section != 1
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        switch editingStyle {
        case .delete:
            removeCellAtIndexPath(indexPath)
            print("delete")
        case .insert:
            print("insert")
        case .none:
            print("NONE")
        default:
            print("DEFAULT")
        }
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let source = autos[sourceIndexPath.row]
        autos.remove(at: sourceIndexPath.row)
        autos.insert(source, at: destinationIndexPath.row)
        print(autos)
    }
}

extension ViewController: AddViewControllerDelegate {
    func didPressSave(car: Car) {
        autos.append(car)
        // FORCE LOAD
//        tableView.reloadData()
        // Reload section
        tableView.reloadSections([0], with: .automatic)
    }
}
