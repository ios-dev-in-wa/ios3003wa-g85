//
//  DetailsViewController.swift
//  ios85ls9
//
//  Created by WA on 29.04.2021.
//

import UIKit

class DetailsViewController: UIViewController {

    @IBOutlet weak var carTitleLabel: UILabel!

    var carTitle: String?

    override func viewDidLoad() {
        super.viewDidLoad()

        carTitleLabel.text = carTitle
    }
}
