//
//  KeyboardViewController.swift
//  ios3003ls17
//
//  Created by WA on 08.06.2021.
//

import UIKit
import AVKit

class KeyboardViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    private let bottomDefaultOffset: CGFloat = 150
    
    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.publisher(for: UIResponder.keyboardDidHideNotification).sink { notification in
            guard let obj = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
            let rect = obj.cgRectValue
            
            self.scrollView.contentInset.bottom = rect.height
        }
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow), name: UIResponder.keyboardDidHideNotification, object: nil)
    }

    override func viewDidAppear(_ animated: Bool) {
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized:
            print("you have access")
        case .denied:
            print("show settings and get access")
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { granted in
                if granted {
                    print("success")
                }
            }
        case .restricted:
            print("show settings and get access")
        @unknown default:
            fatalError()
        }
        
    }

    @objc func keyboardDidShow(notification: Notification) {
        guard let obj = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        let rect = obj.cgRectValue
        
        scrollView.contentInset.bottom = rect.height
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @IBAction func showSettigsAction(_ sender: UIButton) {
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                    return
                }
        UIApplication.shared.open(settingsUrl, options: [:], completionHandler: nil)
    }

    @IBAction func tapAction(_ sender: UITapGestureRecognizer) {
        view.endEditing(true)
    }
}
