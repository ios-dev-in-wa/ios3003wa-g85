//
//  Settings.swift
//  ios3003ls17
//
//  Created by WA on 08.06.2021.
//

import Foundation

final class Settings {

    static let shared = Settings()

    private init() { }

    private let defaults = UserDefaults.standard

    var nameValue: String {
        get {
            return defaults.string(forKey: "name_preference") ?? ""
        }
        set {
            defaults.setValue(newValue, forKey: "name_preference")
        }
    }
}
