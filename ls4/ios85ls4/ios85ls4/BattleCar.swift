//
//  BattleCar.swift
//  ios85ls4
//
//  Created by WA on 13.04.2021.
//

import Foundation

enum GunType: Int {
    case minigun = 10
    case rocketLauncher

    var damage: Double {
        switch self {
        case .minigun: return 10.0
        case .rocketLauncher: return 100.0
        }
    }
}

class BattleCar: Car {
    let gunType: GunType

    init(gunType: GunType) {
        self.gunType = gunType
        super.init(make: "Hummer", bodyType: .universal)
    }

    override func startEngine() {
        super.startEngine()
        print("Tttttt-tttttttRRRR, \(make) is started")
    }

    func fire() {
        switch gunType {
        case .minigun:
            print("Ttttttttt")
        case .rocketLauncher:
            print("Ppppshshsh")
        }
    }
}
