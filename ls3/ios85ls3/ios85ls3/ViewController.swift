//
//  ViewController.swift
//  ios85ls3
//
//  Created by WA on 06.04.2021.
//

import UIKit

//struct Phone {
//    let city: String
//    let phoneNumber: Int
//}

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        let some = 8^3
//        print(some)
//        fromMinToMax(value: 10)
//        print(pow(8, 2))
//        stringExample()

//        arrayExample()
//        minMax(valueOne: 2, valueTwo: 3)
//        minMax(valueOne: 2, valueTwo: 2)
        dictionaryExample()
    }
    
    func minMax(valueOne: Int, valueTwo: Int) {
        if valueTwo == valueOne {
            print("Numbers equal")
            return
        }
        print(valueOne > valueTwo ? valueOne : valueTwo)
    }

    func fromMinToMax(value: Int) {
        for i in 0...value {
            let minValue = i
            let maxValue = value - i
            print("\(minValue) -- \(maxValue)")
        }
    }

    func stringExample() {
        var string = "Artem Velykyy"
        for i in string {
            print(i)
        }
        string.append(" mentor")
        print(string)
        print(string.count)

        if string.hasPrefix("Ar") {
            print("YES, you are right")
        } else {
            print("NO")
        }

        // OPTIONAL BINDING
        if let index = string.firstIndex(of: "A") {
            string.insert("+", at: index)
        }
        print(string)

        let newString = string.replacingOccurrences(of: "r", with: "RrRrR")
        print(string, newString)
    }

    func arrayExample() {
        var array = [6, 1, 12, 3, 5]

        array.append(220)
        array.remove(at: 0)
        for i in array {
            print(i)
        }
        array.insert(6, at: 0)

        array += [10, 11]

        let filtereArray = array.filter { i -> Bool in
            let value = i % 2
            let filtering = value == 0
            return filtering
        }
        print(filtereArray)

        print(array.sorted(by: <))
        print(array.sorted(by: >))
        let sortedArray = array.sorted { valueOne, valueTwo -> Bool in
            return valueOne > valueTwo
        }
        print(sortedArray)

        let stringArray = array.map { i -> String in
            return String(i)
        }
        print(stringArray)

        // WARNING, [N] - N не должен быть больше чем кол-во элементов в массиве
        let first = array[0]
        let hundred = 100

        if array.count > 100 {
            print(array[hundred])
        }

//        array.removeAll()
        let ternaryValue = array.isEmpty ? 100 : 0
        print(ternaryValue)

        var doubleArray: [Double] = []
        doubleArray.append(22.01)
    }

    func dictionaryExample() {
        
//        var dict: [String: [Phone]] = [
//            "Police": [
//                Phone(city: "Kiev", phoneNumber: 343243),
//                Phone(city: "Odessa", phoneNumber: 123123)
//            ]
//        ]
        var dictionary = [
            "Police": 0404040,
            "Embulance": 1010101,
            "My numbre": 100020
        ]

        let dictIntKey = [
            0: "Zero",
            1: "One",
            2: "Two"
        ]

        print(dictionary["Police333"])

        dictionary["Mama"] = 9002020
        dictionary["Police"] = 911
        print(dictionary)
    
        print(dictionary.count)
        print(dictionary.keys)

        print(dictionary.values)
    }

}

