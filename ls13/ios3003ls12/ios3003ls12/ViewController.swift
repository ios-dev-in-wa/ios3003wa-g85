//
//  ViewController.swift
//  ios3003ls12
//
//  Created by WA on 20.05.2021.
//

import UIKit

struct Car: Codable {
    let make: String
    let productionYear: Int
    let wasInAccident: Bool
    let objectId: String?
}

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    let service: CarServiceProtocol = RESTCarService()// CarService()
    let control = UIRefreshControl()

    var cars = [Car]()
//        Car(name: "Mazda", productionYear: 1999),
//        Car(name: "Benz", productionYear: 2020),
//        Car(name: "BMW", productionYear: 2012),
    

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self

        tableView.refreshControl = control
        control.addTarget(self, action: #selector(refreshData), for: .valueChanged)


        let nib = UINib(nibName: "MainTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "MainTableViewCell")

        refreshData()
    }

    @objc func refreshData() {
        service.readCars { result in
            switch result {
            case .success(let cars):
                print(cars)
                self.cars = cars
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.control.endRefreshing()
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToAddCar",
           let vc = segue.destination as? AddCarViewController {
            vc.carCreated = { car in
                self.service.createCar(car: car) { result in
                    switch result {
                    case .success(let car):
                        self.cars.append(car)
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                        }
                    case .failure(let error):
                        print(error.localizedDescription)
                    }
                }
            }
        }
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cars.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MainTableViewCell", for: indexPath) as? MainTableViewCell else {
            fatalError()
        }
        let currentCar = cars[indexPath.row]
        cell.mainLabel.text = currentCar.make
        cell.secondaryLabel.text = "\(currentCar.productionYear) year"
        return cell
    }

    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let action = UIContextualAction(style: .destructive,
                                        title: "Remove") { [weak self] (_, _, _) in
            // TODO: - removing from db
            let id = self?.cars[indexPath.row].objectId ?? ""
            self?.service.deleteCar(carObjectId: id, completion: { result in
                switch result {
                case .failure(let error):
                    print(error.localizedDescription)
                case .success:
                    self?.cars.remove(at: indexPath.row)
                    DispatchQueue.main.async {
                        self?.tableView.reloadSections([0], with: .automatic)
                    }
                }
            })
        }
        return UISwipeActionsConfiguration(actions: [action])
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let navC = storyboard.instantiateViewController(withIdentifier: "DetailsNavC") as? UINavigationController,
              let detailsVC = navC.viewControllers.first as? DetailsViewController else { return }
        detailsVC.car = cars[indexPath.row]
        detailsVC.carDidUpdate = { [weak self] car in
            self?.service.updateCar(car: car, completion: { result in
                switch result {
                case .failure(let error):
                    print(error.localizedDescription)
                case .success(let newCar):
                    guard let index = self?.cars.firstIndex(where: { $0.objectId == newCar.objectId }) else { return }
                    self?.cars.remove(at: index)
                    self?.cars.insert(newCar, at: index)
                    DispatchQueue.main.async {
                        self?.tableView.reloadSections([0], with: .automatic)
                    }
                }
            })
        }

        splitViewController?.showDetailViewController(navC, sender: nil)
//        performSegue(withIdentifier: "showDetails", sender: cars[indexPath.row])
    }
}
