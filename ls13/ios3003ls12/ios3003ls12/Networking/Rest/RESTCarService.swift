//
//  RESTCarService.swift
//  ios3003ls12
//
//  Created by WA on 25.05.2021.
//

import Foundation

// URLSession implementation
class RESTCarService: CarServiceProtocol {

    private let session = URLSession.shared
    private let baseUrl = URL(string: "https://parseapi.back4app.com/classes/Car")!

    func createCar(car: Car, completion: @escaping ((Result<Car, Error>) -> Void)) {
        
        let body = try? JSONEncoder().encode(car)
        let request = createRequest(url: baseUrl, method: "POST", body: body)
        
        session.dataTask(with: request) { data, response, error in
            if let error = error {
                completion(.failure(error))
                return
            }

            guard let httpResponse = response as? HTTPURLResponse else {
                completion(.failure(CarsError.noCars))
                return
            }
            if httpResponse.statusCode == 201 {
                guard let data = data else { return }
                do {
                    let carsResponse = try JSONDecoder().decode(POSTCarResponse.self, from: data)
                    completion(.success(Car(make: car.make,
                                            productionYear: car.productionYear,
                                            wasInAccident: car.wasInAccident,
                                            objectId: carsResponse.objectId)))
                } catch {
                    completion(.failure(error))
                }
            } else {
                completion(.failure(CarsError.noCars))
            }
        }.resume()
    }
    
    func readCars(completion: @escaping ((Result<[Car], Error>) -> Void)) {
        let request = createRequest(url: baseUrl, method: "GET", body: nil)

        session.dataTask(with: request) { data, response, error in
            if let error = error {
                completion(.failure(error))
                return
            }

            guard let httpResponse = response as? HTTPURLResponse else {
                completion(.failure(CarsError.noCars))
                return
            }
            if httpResponse.statusCode == 200, let data = data {
                do {
                    let response = try JSONDecoder().decode(GETCarsResponse.self, from: data)
                    completion(.success(response.results))
                } catch {
                    completion(.failure(error))
                }
            }
        }.resume()
    }
    
    func updateCar(car: Car, completion: @escaping ((Result<Car, Error>) -> Void)) {
        let body = try? JSONEncoder().encode(PUTCarRequest(make: car.make, productionYear: car.productionYear, wasInAccident: car.wasInAccident))
        let request = createRequest(url: baseUrl.appendingPathComponent(car.objectId ?? ""), method: "PUT", body: body)

        session.dataTask(with: request) { data, response, error in
            if let error = error {
                completion(.failure(error))
                return
            }

            guard let httpResponse = response as? HTTPURLResponse else {
                completion(.failure(CarsError.noCars))
                return
            }
            if httpResponse.statusCode == 200 {
                completion(.success(car))
            }
        }.resume()
    }
    
    func deleteCar(carObjectId: String, completion: @escaping ((Result<Void, Error>) -> Void)) {
        let request = createRequest(url: baseUrl.appendingPathComponent(carObjectId), method: "DELETE", body: nil)

        session.dataTask(with: request) { data, response, error in
            if let error = error {
                completion(.failure(error))
                return
            }

            guard let httpResponse = response as? HTTPURLResponse else {
                completion(.failure(CarsError.noCars))
                return
            }
            if httpResponse.statusCode == 200 {
                completion(.success(()))
            }
        }.resume()
    }

    private func createRequest(url: URL, method: String, body: Data?) -> URLRequest {
        var request = URLRequest(url: url)
        request.httpMethod = method

        request.addValue("3SlfgQ3RaElMyq4vSelrv5uKSFtukXSRYGQ6T03k", forHTTPHeaderField: "X-Parse-Application-Id")
        request.addValue("JHwW5e9EWOCCzU31UWTJjJM8nMrO4lZbeV5KG9os", forHTTPHeaderField: "X-Parse-REST-API-Key")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")

        return request
    }
}
