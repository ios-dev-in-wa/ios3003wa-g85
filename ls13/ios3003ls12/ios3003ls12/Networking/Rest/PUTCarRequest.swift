//
//  PUTCarRequest.swift
//  ios3003ls12
//
//  Created by WA on 25.05.2021.
//

import Foundation

struct PUTCarRequest: Codable {
    let make: String
    let productionYear: Int
    let wasInAccident: Bool
}
