//
//  AddCarViewController.swift
//  ios3003ls12
//
//  Created by WA on 20.05.2021.
//

import UIKit

class AddCarViewController: UIViewController {

    @IBOutlet weak var makeTextField: UITextField!
    @IBOutlet weak var productionYearTextfield: UITextField!
    @IBOutlet weak var wasInAccidentSwitch: UISwitch!

    var car: Car?
    var carCreated: ((Car) -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        if let car = car {
            makeTextField.text = car.make
            productionYearTextfield.text = "\(car.productionYear)"
            wasInAccidentSwitch.isOn = car.wasInAccident
        }
    }

    @IBAction func saveAction(_ sender: UIButton) {
        guard let make = makeTextField.text,
              let year = productionYearTextfield.text else { return }
        carCreated?(Car(make: make,
                        productionYear: Int(year) ?? 2000,
                        wasInAccident: wasInAccidentSwitch.isOn,
                        objectId: car?.objectId))
        if let navC = navigationController {
            navC.popViewController(animated: true)
        } else {
            dismiss(animated: true, completion: nil)
        }
    }
}
