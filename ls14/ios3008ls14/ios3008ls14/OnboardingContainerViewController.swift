//
//  OnboardingContainerViewController.swift
//  ios3008ls14
//
//  Created by WA on 27.05.2021.
//

import UIKit

class OnboardingContainerViewController: UIViewController {

    private var pageViewController: UIPageViewController?
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    
    let onboardingModels = [
        OnboardingModel(imageName: "onboarding-1",
                        headerText: "Let us deliver you great meal!".localized,
                        mainText: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."),
        OnboardingModel(imageName: "onboarding-2",
                        headerText: "Let us deliver you great meal!",
                        mainText: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."),
        OnboardingModel(imageName: "onboarding-3",
                        headerText: "Let us deliver you great meal!",
                        mainText: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."),
        OnboardingModel(imageName: "",
                        headerText: "Thanks for choosing our App",
                        mainText: "BLA BLA BLA")
    ]

    // Lazy означает что этот контейнер будет создан только тогда когда к нему обратятся
    lazy var controllers: [UIViewController] = {
        let controllers = onboardingModels.compactMap { model -> UIViewController? in
            let vc = storyboard?.instantiateViewController(withIdentifier: "OnboardingViewController") as? OnboardingViewController
            vc?.onboardingModel = model
            return vc
        }
        return controllers
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        pageViewController?.delegate = self
        pageViewController?.dataSource = self

        pageViewController?.setViewControllers([controllers.first!], direction: .forward, animated: false, completion: nil)
        pageControl.numberOfPages = controllers.count
        updateButtonTitle(controller: controllers.first!)

//        let test: [UIViewController] = {
//            let controllers = self.onboardingModels.compactMap { model -> UIViewController? in
//                let vc = self.storyboard?.instantiateViewController(withIdentifier: "OnboardingViewController") as? OnboardingViewController
//                vc?.onboardingModel = model
//                return vc
//            }
//            return controllers
//        }()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showPageVC",
           let pageVC = segue.destination as? UIPageViewController {
            pageViewController = pageVC
        }
    }

    func updateButtonTitle(controller: UIViewController) {
        let title = controller == controllers.last ? "Finish".localized : "Skip".localized
        skipButton.setTitle(title, for: .normal)
    }

    func showController(controller: UIViewController, direction: UIPageViewController.NavigationDirection) {
        pageViewController?.setViewControllers([controller], direction: direction, animated: true, completion: nil)
        updateButtonTitle(controller: controller)
    }

    @IBAction func skipAction(_ sender: Any) {
        guard let currentVC = pageViewController?.viewControllers?.first,
              let index = controllers.firstIndex(of: currentVC) else { return }
        if index + 1 < controllers.count {
            let controller = controllers[index + 1]
            showController(controller: controller, direction: .forward)
            pageControl.currentPage = index + 1
        } else {
            Settings.shared.ifOnboardingShown = true
            print("Reached end of onboarding")
            SceneDelegate.sceneDelegate?.updateRoot()
//            dismiss(animated: true, completion: nil)
        }
    }

    @IBAction func pageControlValueChanged(_ sender: UIPageControl) {
        let controller = controllers[sender.currentPage]
        guard let currentVC = pageViewController?.viewControllers?.first,
              let index = controllers.firstIndex(of: currentVC) else { return }
        let direction: UIPageViewController.NavigationDirection = index > sender.currentPage ? .reverse : .forward
        showController(controller: controller, direction: direction)
    }
}

extension OnboardingContainerViewController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        guard let lastVC = pendingViewControllers.first else { return }
        if let index = controllers.firstIndex(of: lastVC) {
            pageControl.currentPage = index
        }
        updateButtonTitle(controller: lastVC)
    }

    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        guard let lastVC = pageViewController.viewControllers?.first else { return }
        if let index = controllers.firstIndex(of: lastVC) {
            pageControl.currentPage = index
        }
    }
}

extension OnboardingContainerViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let index = controllers.firstIndex(of: viewController) else {
            return nil
        }
        if index == 0 {
            return nil
        }
        return controllers[index - 1]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let index = controllers.firstIndex(of: viewController) else {
            return nil
        }
        guard index + 1 < controllers.count else { return nil }
        return controllers[index + 1]
    }
}


extension SceneDelegate {
    static var sceneDelegate: SceneDelegate? {
        guard let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene,
            let delegate = windowScene.delegate as? SceneDelegate else { return nil }
         return delegate
    }
}
