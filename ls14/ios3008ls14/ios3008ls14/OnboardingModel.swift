//
//  OnboardingModel.swift
//  ios3008ls14
//
//  Created by WA on 27.05.2021.
//

import Foundation

struct OnboardingModel {
    let imageName: String
    let headerText: String
    let mainText: String
}
