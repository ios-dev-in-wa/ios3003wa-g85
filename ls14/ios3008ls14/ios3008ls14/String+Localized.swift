//
//  String+Localized.swift
//  ios3008ls14
//
//  Created by WA on 27.05.2021.
//

import Foundation

extension String {
    var localized: String {
        NSLocalizedString(self, comment: "")
    }
}
