//
//  ViewController.swift
//  ios3008ls14
//
//  Created by WA on 27.05.2021.
//

import UIKit

class OnboardingViewController: UIViewController {

    @IBOutlet weak var imageVIew: UIImageView!
    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!

    var onboardingModel: OnboardingModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }

    private func setupUI() {
        guard let model = onboardingModel else { return }
        imageVIew.image = UIImage(named: model.imageName)
        mainLabel.text = model.headerText
        subtitleLabel.text = model.mainText
    }
}
