//
//  Settings.swift
//  ios3008ls14
//
//  Created by WA on 27.05.2021.
//

import Foundation

final class Settings {
    private init() { }

    static let shared = Settings()

    private let userDefaults = UserDefaults.standard

    var ifOnboardingShown: Bool {
        get {
            userDefaults.bool(forKey: "ifOnboardingNeeded")
        }
        set {
            userDefaults.setValue(newValue, forKey: "ifOnboardingNeeded")
        }
    }
}
