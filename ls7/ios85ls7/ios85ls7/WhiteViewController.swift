//
//  WhiteViewController.swift
//  ios85ls7
//
//  Created by WA on 22.04.2021.
//

import UIKit

protocol WhiteVCDelegate: class {
    func counterValueAdded()
    func textFieldValueChanged(text: String)
    func counterValueChanged(value: Int)
}

class WhiteViewController: UIViewController {

    @IBOutlet weak var counterLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    
    var counter: Int?
    var apparat: CoffeeMachine?
    var counterValueChangedHadler: ((Int) -> Void)?

    weak var delegate: WhiteVCDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        if let counter = counter {
            counterLabel.text = "\(counter) from gray VC"
        }
        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        apparat?.makeALatte()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        delegate?.counterValueChanged(value: counter ?? -1)
        counterValueChangedHadler?(counter ?? -1)
    }

    func printHello() {
        print("HEllo")
    }
    
    @IBAction func editingChangedAction(_ sender: UITextField) {
        delegate?.textFieldValueChanged(text: sender.text ?? "--")
    }
    
    @IBAction func addCountAction(_ sender: UIButton) {
        if var counter = counter {
            counter += 1
            counterLabel.text = "\(counter) from gray VC"
            self.counter = counter
//            delegate?.counterValueAdded()
        }
        
    }

    @IBAction func popButtonAction(_ sender: UIBarButtonItem) {
//        navigationController?.popViewController(animated: true)
        let blueViewController = UIViewController()
        _ = blueViewController.view
        blueViewController.view.backgroundColor = .blue
        navigationController?.pushViewController(blueViewController, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
