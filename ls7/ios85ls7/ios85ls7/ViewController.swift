//
//  ViewController.swift
//  ios85ls7
//
//  Created by WA on 22.04.2021.
//

import UIKit

class CoffeeMachine {
    func makeALatte() {
        print("Your latte, sir")
    }
}

class ViewController: UIViewController {

    @IBOutlet weak var counterLabel: UILabel!
    
    var counter: Int = 0
    let coffeeMachine = CoffeeMachine()
    let nature = Nature()

    override func viewDidLoad() {
        super.viewDidLoad()
        let human = Human()
        let rabbit = Rabbit()
        let bear = Bear()
        nature.seasonbles = [human, rabbit, bear]
        nature.live()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        counter = 0
//        counterLabel.text = "\(counter)"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // to show additional controllers, alerts
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        // Destination View Controller is ready, but view is NOT loaded yet!
        if segue.identifier == "toWhiteVC", let whiteVC = segue.destination as? WhiteViewController {
            // to force load view
            // _ = whiteVC.view
            whiteVC.counter = counter
            whiteVC.printHello()
            whiteVC.apparat = coffeeMachine
//            whiteVC.delegate = self
            whiteVC.counterValueChangedHadler = { value in
                self.counter = value
                self.counterLabel.text = "\(self.counter)"
                print("VALUE OF COUNTER IS CHANGED", self.counter)
            }
        }
    }


    @IBAction func addCountAction(_ sender: UIButton) {
        counter += 1
        counterLabel.text = "\(counter)"
        if counter % 5 == 0 {
            performSegue(withIdentifier: "toWhiteVC", sender: nil)
        }
    }
}

extension ViewController: WhiteVCDelegate {
    func counterValueChanged(value: Int) {
        counter = value
        counterLabel.text = "\(counter)"
        print("VALUE OF COUNTER IS CHANGED", counter)
    }
    
    func textFieldValueChanged(text: String) {
        print(text)
    }
    
    func counterValueAdded() {
        counter += 1
        counterLabel.text = "\(counter)"
    }
}
