//
//  Season.swift
//  ios85ls7
//
//  Created by WA on 22.04.2021.
//

import UIKit

protocol Seasonable: class {
    func winterIsComming()
    func springIsComming()
    func summerIsComming()
    func autumnIsComming()
}

protocol Predator {
    func searchForFood()
    func killSmallerAnimal()
}

class Bear: Seasonable, Predator {
    var isSleeping: Bool = false
    var fatVolume: Double = 1
    
    func searchForFood() {
        
    }
    
    func killSmallerAnimal() {
        
    }

    func winterIsComming() {
        print("Bear is sleeping")
        fatVolume -= 1
        isSleeping = true
    }
    
    func springIsComming() {
        print("Bear waked and hunting")
        fatVolume += 0.3
        isSleeping = false
    }
    
    func summerIsComming() {
        print("Bear hunting")
        fatVolume += 0.3
    }
    
    func autumnIsComming() {
        print("Bear hunting, making children")
        fatVolume += 0.4
    }
}

class Rabbit: Seasonable {
    var furColor: UIColor = .white
    var speed: Double = 1

    func winterIsComming() {
        print("Rabbit changing fur color, get max speed")
        speed = 1
        furColor = .white
    }
    
    func springIsComming() {
        print("Rabbit changing fur color to normal, loosing speed")
        speed -= 0.1
        furColor = .gray
    }
    
    func summerIsComming() {
        print("loosing speed")
        speed -= 0.3
    }

    func autumnIsComming() {
        print("Rabbit changing fur color to normal, loosing speed")
        speed -= 0.3
    }
}

class Human: Seasonable, Predator {
    func searchForFood() {
        
    }
    
    func killSmallerAnimal() {
        
    }
    
    func winterIsComming() {
        print("Human Fireplace loading")
    }
    
    func springIsComming() {
        print("Human Garding")
    }
    
    func summerIsComming() {
        print("Human chilling")
    }
    
    func autumnIsComming() {
        print("Human Gathering")
    }
}

class Nature {
    var seasonbles: [Seasonable] = []

    func live() {
        seasonbles.forEach { $0.winterIsComming() }
        seasonbles.forEach { $0.springIsComming() }
        seasonbles.forEach { $0.summerIsComming() }
        seasonbles.forEach { $0.autumnIsComming() }
//        for i in seasonbles {
//            i.winterIsComming()
//            i.springIsComming()
//            i.summerIsComming()
//            i.autumnIsComming()
//        }
    }
}
