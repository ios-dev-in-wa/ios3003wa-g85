//
//  CoreDataStorage.swift
//  ios3003ls12
//
//  Created by WA on 03.06.2021.
//

import CoreData
import UIKit

class CarCoreDataStorage: CarServiceProtocol {
    
    private var context: NSManagedObjectContext {
        (UIApplication.shared.delegate as? AppDelegate)!.managedContext
    }

    func createCar(car: Car, completion: @escaping ((Result<Car, Error>) -> Void)) {
        guard let entity = NSEntityDescription.entity(forEntityName: "CarEntity", in: context) else { return }
        guard let carEntity = NSManagedObject(entity: entity, insertInto: context) as? CarEntity else { return }
        carEntity.make = car.make
        carEntity.productionYear = Int64(car.productionYear)
        carEntity.wasInAccident = car.wasInAccident
        carEntity.uniqueKey = car.objectId

        do {
            try context.save()
            completion(.success(car))
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func readCars(completion: @escaping ((Result<[Car], Error>) -> Void)) {
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "CarEntity")

        do {
            guard let data = try context.fetch(fetchRequest) as? [CarEntity] else { return }
            let cars = data.map { entity in
                return Car(make: entity.make ?? "",
                           productionYear: Int(entity.productionYear),
                           wasInAccident: entity.wasInAccident ,
                           objectId: entity.uniqueKey ?? "",
                           ownerName: entity.owner?.name)
            }
            completion(.success(cars))
        } catch {
            
        }
    }
    
    func updateCar(car: Car, completion: @escaping ((Result<Car, Error>) -> Void)) {
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "CarEntity")

        fetchRequest.predicate = NSPredicate(format: "objectId == %@", car.objectId!)

        do {
            guard let carEntities = try context.fetch(fetchRequest) as? [CarEntity],
                  let carEntity = carEntities.first else { return }
            carEntity.make = car.make
            carEntity.productionYear = Int64(car.productionYear)
            carEntity.wasInAccident = car.wasInAccident
            try context.save()
            completion(.success(car))
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func deleteCar(carObjectId: String, completion: @escaping ((Result<Void, Error>) -> Void)) {
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "CarEntity")

        fetchRequest.predicate = NSPredicate(format: "objectId == %@", carObjectId)
        do {
            guard let carEntities = try context.fetch(fetchRequest) as? [CarEntity],
                  let carEntity = carEntities.first else { return }
            context.delete(carEntity)
            try context.save()
            completion(.success(()))
        } catch {
            print(error.localizedDescription)
        }
    }
}
