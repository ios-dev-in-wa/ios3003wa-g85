//
//  UserCoreDataStorage.swift
//  ios3003ls12
//
//  Created by WA on 03.06.2021.
//

import CoreData
import UIKit

class UserCoreDataStorage {
    private var context: NSManagedObjectContext {
        (UIApplication.shared.delegate as? AppDelegate)!.managedContext
    }

    func createUser(name: String) {
        guard let entity = NSEntityDescription.entity(forEntityName: "User", in: context) else { return }
        guard let user = NSManagedObject(entity: entity, insertInto: context) as? User else { return }
        user.name = name
        do {
            try context.save()
//            completion(.success(car))
        } catch {
            print(error.localizedDescription)
        }
    }

    func addCar(car: Car, toUser withName: String) {
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "User")

        fetchRequest.predicate = NSPredicate(format: "name == %@", withName)
        
        ///
        let carFetchRequest = NSFetchRequest<NSManagedObject>(entityName: "CarEntity")

        carFetchRequest.predicate = NSPredicate(format: "objectId == %@", car.objectId!)

        do {
            guard let user = (try context.fetch(fetchRequest) as? [User])?.first,
                  let car = (try context.fetch(carFetchRequest) as? [CarEntity])?.first else { return }
            user.addToCars(car)
            car.owner = user
            
            try context.save()
        } catch {
            print(error.localizedDescription)
        }
    }
}
