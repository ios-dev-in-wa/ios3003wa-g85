//
//  DetailsViewController.swift
//  ios3003ls12
//
//  Created by WA on 20.05.2021.
//

import UIKit

class DetailsViewController: UIViewController {

    @IBOutlet weak var makeLabel: UILabel!
    @IBOutlet weak var productionYearLabel: UILabel!
    @IBOutlet weak var mainStackView: UIStackView!
    @IBOutlet weak var emptyStateLabel: UILabel!
    @IBOutlet weak var wasInAccidentLAbel: UILabel!
    @IBOutlet weak var ownerLabel: UILabel!
    
    var car: Car?
    var carDidUpdate: ((Car) -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
//        traitCollection
        print(UIDevice.current.name, UIDevice.current.model)
        print(UIDevice.current.userInterfaceIdiom)

//        Locale.isoRegionCodes.forEach {
//            print(Locale.current.localizedString(forRegionCode: $0))
//        }
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        print("traitCollectionDidChange")
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showEditCarDetails", let vc = segue.destination as? AddCarViewController {
            vc.car = car
            vc.carCreated = { [weak self] car in
                self?.car = car
                self?.setupUI()
                self?.carDidUpdate?(car)
            }
        }
    }

    func setupUI() {
        makeLabel.text = car?.make
        if let year = car?.productionYear {
            productionYearLabel.text = "\(year)"
            emptyStateLabel.isHidden = true
            mainStackView.isHidden = false
        }
        ownerLabel.text = car?.ownerName
        wasInAccidentLAbel.text = car?.wasInAccident == true ? "Yes" : "No"
    }
}
