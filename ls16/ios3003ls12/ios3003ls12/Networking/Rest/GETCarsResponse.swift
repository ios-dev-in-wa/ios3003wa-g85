//
//  GETCarsResponse.swift
//  ios3003ls12
//
//  Created by WA on 25.05.2021.
//

import Foundation

struct GETCarsResponse: Codable {
    let results: [Car]
}
