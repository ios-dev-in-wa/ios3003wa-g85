//
//  DetailsModel.swift
//  ios3003ls12
//
//  Created by WA on 20.05.2021.
//

import Foundation
import Parse

enum CarsError: Error {
    case noCars, cantDeleteCar
}

protocol CarServiceProtocol {
    func createCar(car: Car, completion: @escaping ((Result<Car, Error>) -> Void))
    func readCars(completion: @escaping ((Result<[Car], Error>) -> Void))
    func updateCar(car: Car, completion: @escaping ((Result<Car, Error>) -> Void))
    func deleteCar(carObjectId: String, completion: @escaping ((Result<Void, Error>) -> Void))
}

// PARSE SDK Implementation
class CarService {
    // Result<Type, Error>
    // Type - success
    // Error - ошибку
    func readCars(completion: @escaping ((Result<[Car], Error>) -> Void)) {
        let query = PFQuery(className: "Car")

        query.findObjectsInBackground { (objects, error) in
            if let error = error {
                print(error.localizedDescription)
                completion(.failure(error))
                return
            }
            guard let objects = objects else { return }
            var cars = [Car]()
            for object in objects {
                guard let car = self.createCar(object: object) else {
                    return
                }
                cars.append(car)
            }
            completion(.success(cars))
//            delegate?.didLoadCars(cars)
        }
    }

    func updateCar(car: Car, completion: @escaping ((Result<Car, Error>) -> Void)) {
        let query = PFQuery(className: "Car")

        query.getObjectInBackground(withId: car.objectId ?? "", block: { object, error in
            if let error = error {
                completion(.failure(error))
                return
            }
            object?["make"] = car.make
            object?["productionYear"] = car.productionYear
            object?["wasInAccident"] = car.wasInAccident

            object?.saveInBackground(block: { _, error in
                if let error = error {
                    completion(.failure(error))
                    return
                }
                guard let obj = object, let newCar = self.createCar(object: obj) else { return }
                completion(.success(newCar))
            })
        })
    }

    func deleteCar(carObjectId: String, completion: @escaping ((Result<Void, Error>) -> Void)) {
        let query = PFQuery(className: "Car")

        query.getObjectInBackground(withId: carObjectId) { (object, error) in
            if let error = error {
                completion(.failure(error))
                return
            }
            object?.deleteInBackground(block: { (isDeleted, error) in
                if let error = error {
                    completion(.failure(error))
                    return
                }
                if isDeleted {
                    completion(.success(()))
                } else {
                    completion(.failure(CarsError.cantDeleteCar))
                }
            })
        }
    }

    func createCar(car: Car, completion: @escaping ((Result<Car, Error>) -> Void)) {
        let parseObject = PFObject(className:"Car")

        parseObject["make"] = car.make
        parseObject["productionYear"] = car.productionYear
        parseObject["wasInAccident"] = car.wasInAccident

        parseObject.saveInBackground { (isSuccess, error) in
            if let error = error {
                completion(.failure(error))
                return
            }
            guard let car = self.createCar(object: parseObject) else { return }
            completion(.success(car))
        }
    }

    private func createCar(object: PFObject) -> Car? {
        guard let make = object["make"] as? String,
              let productionYear = object["productionYear"] as? Int,
              let wasInAccident = object["wafsInAccident"] as? Bool,
              let objectId = object.objectId else { return nil}
        return Car(make: make,
                   productionYear: productionYear,
                   wasInAccident: wasInAccident,
                   objectId: objectId,
                   ownerName: nil)
    }
}
