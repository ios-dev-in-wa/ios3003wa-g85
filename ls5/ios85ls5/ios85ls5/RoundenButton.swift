//
//  RoundenButton.swift
//  ios85ls5
//
//  Created by WA on 15.04.2021.
//

import UIKit

class RoundedButton: UIButton {

    init() {
        super.init(frame: CGRect.zero)
        layer.cornerRadius = 10
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
