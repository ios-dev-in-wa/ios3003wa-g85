//
//  Car.swift
//  ios85ls5
//
//  Created by WA on 15.04.2021.
//

import UIKit

class Car {
    let title: String
    let color: UIColor
    var dirtyLevel: Double = 0.0

    init(title: String, color: UIColor) {
        self.title = title
        self.color = color
    }
    
    deinit {
        print("\(title) deinited")
    }
}
