//
//  CarWash.swift
//  ios85ls5
//
//  Created by WA on 15.04.2021.
//

import Foundation

class CarWash {
    private var waterTank: Double = 1000
    private var soapTank: Double = 100
    
    func washCar(_ car: Car) {
        if waterTank >= 100, soapTank >= 5 {
            waterTank -= 100
            soapTank -= 5
            car.dirtyLevel = 0
            print("\(car.title) is clear")
        } else {
            print("Car can't be washed!")
        }
    }
}
