//
//  ViewController.swift
//  ios85ls5
//
//  Created by WA on 15.04.2021.
//

import UIKit

//enum DrinkType {
//    case tea, espresso, americano
//
//    var coffeeNeeded: Double {
//        switch self {
//        case .tea: return 0
//        case .americano: return 10
//        case .espresso: return 10
//        }
//    }
//
//    var waterNeeded: Double {
//        switch self {
//        case .tea: return 300
//        case .americano: return 300
//        case .espresso: return 50
//        }
//    }
//}
//
//func makeADrink(_ drink: DrinkType) {
//    if waterTank > drink.waterNeeded {
//
//    }
//}

class ViewController: UIViewController {
    
    @IBOutlet weak var boxView: UIView!
    @IBOutlet weak var centralButton: UIButton!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var rightButton: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var carTitleLabel: UILabel!
    @IBOutlet weak var decreaseButton: UIButton!
    @IBOutlet weak var increaseButton: UIButton!
    
    var carWash = CarWash()
    var carCounter = 0
    var counter = 0
    var cars = [
        Car(title: "Mazda", color: .black),
        Car(title: "BMW", color: .green),
        Car(title: "BMW2", color: .red),
        Car(title: "BMW3", color: .systemPink),
        Car(title: "BMW4", color: .brown),
    ]
    var car: Car = Car(title: "Mazda", color: .blue)
    weak var carContainer: Car?

    override func viewDidLoad() {
        super.viewDidLoad()
        carContainer = car
//        car = nil
//        carContainer = nil
    
        carWash.washCar(car)
        boxView.frame = CGRect(x: 10, y: 10, width: 300, height: 300)

        let roundedButton = RoundedButton()
        roundedButton.backgroundColor = .green
        roundedButton.frame = CGRect(x: 100, y: 200, width: 100, height: 40)

//        view.addSubview(roundedButton)

        if let car = cars.first {
            updateUIWith(car)
        }
    }

    func updateUIWith(_ car: Car) {
        imageView.tintColor = car.color
        carTitleLabel.text = car.title

//        decreaseButton.isEnabled = carCounter > 0
//        increaseButton.isEnabled = carCounter < cars.count - 1
    }

    @IBAction func increaseStep(_ sender: UIButton) {
        if carCounter == cars.count - 1 {
            carCounter = 0
        } else {
            carCounter += 1
        }
//        guard carCounter < cars.count - 1 else { return }
        let currentCar = cars[carCounter]

        updateUIWith(currentCar)
    }

    @IBAction func decreaseStep(_ sender: UIButton) {
        // If inifinite is needed
        if carCounter == 0 {
            carCounter = cars.count - 1
        } else {
            carCounter -= 1
        }
//        guard carCounter > 0 else { return }
        
        let currentCar = cars[carCounter]

        updateUIWith(currentCar)
    }
    
    @IBAction func buttonAction(_ sender: UIButton) {
        counter += 1
        if counter == 3 {
            leftButton.isEnabled = true
        }
        centralButton.setTitle("\(counter)", for: .normal)
        sender.backgroundColor = .red
        boxView.backgroundColor = .cyan
    }
    
}

