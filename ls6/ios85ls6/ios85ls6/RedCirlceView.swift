//
//  RedCirlceView.swift
//  ios85ls6
//
//  Created by WA on 20.04.2021.
//

import UIKit

class RedCirlceView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .red
        layer.cornerRadius = frame.height / 2
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
