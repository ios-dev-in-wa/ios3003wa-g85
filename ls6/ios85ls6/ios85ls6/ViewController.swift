//
//  ViewController.swift
//  ios85ls6
//
//  Created by WA on 20.04.2021.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var firstTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        nameSeparator(text: "ArtemVelykyy")
        filter(searchWord: "da")
        filter(searchWord: "an")
        addCommas(number: "123456789")
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        for i in 0...10 {
            let box = RedCirlceView(frame: CGRect(x: 5, y: CGFloat(20 * i) + 40, width: 20, height: 20))
            view.addSubview(box)
        }
    }

    // "ArtemVelykyy"
    func nameSeparator(text: String) {
        for char in text {
            if char.isUppercase {
                String(char)
                guard let index = text.lastIndex(where: { $0 == char }) else { return }
                print(text[text.startIndex..<index])
                print(text[index..<text.endIndex])
            }
        }

//        let index = text.lastIndex(where: { $0.isUppercase })
//        let separator = text[index!]
//        print(text.split(separator: text[index!]))
    }

    // "da"
    func filter(searchWord: String) {
        let arrayOfStrings = ["lada", "sedan", "baklazhan"]

        var arrayResults = [String]()

        for item in arrayOfStrings {
            if item.lowercased().contains(searchWord.lowercased()) {
                arrayResults.append(item)
            }
        }
        print(arrayResults)
    }

    func addCommas(number: String) {
        var result = ""
        for (index, element) in number.reversed().enumerated() {
            result.insert(element, at: result.startIndex)
            if ((index + 1) % 3 == 0 && index != number.count - 1) {
                result.insert(",", at: result.startIndex)
            }
        }
//        for element in number.reversed() {
//            guard let index = number.firstIndex(where: { $0 == element }) else { return }
//            result.insert(element, at: result.startIndex)
//            if ((index + 1) % 3 == 0 && index != number.count - 1) {
//                result.insert(",", at: result.startIndex)
//            }
//        }
        print(result)
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        view.backgroundColor = .random
    }
    
    @IBAction func textFieldEditingBegin(_ sender: UITextField) {
        sender.layer.borderColor = UIColor.green.cgColor
        sender.layer.borderWidth = 1
    }

    @IBAction func textFieldChanged(_ sender: UITextField) {
        if sender.text == "123" {
            sender.layer.borderColor = UIColor.red.cgColor
        } else {
            sender.layer.borderColor = UIColor.green.cgColor
        }
        if sender == firstTextField, sender.text == "access" {
            textField.isEnabled = true
        }
        print(sender.text)
    }
    
    @IBAction func textFieldDidEndEditing(_ sender: UITextField) {
        sender.layer.borderWidth = 0
    }

    @IBAction func numberButtonAction(_ sender: UIButton) {
        guard let text = textField.text, let senderText = sender.currentTitle else { return }
        textField.text = text + senderText
    }
}

extension CGFloat {
    static var random: CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}

extension UIColor {
    static var random: UIColor {
        return UIColor(red: .random, green: .random, blue: .random, alpha: 1.0)
    }
}
