//
//  UserTableViewCell.swift
//  ios3003ls15
//
//  Created by WA on 01.06.2021.
//

import UIKit
import Kingfisher

class UserTableViewCell: UITableViewCell {

    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    func setupWith(_ user: User) {
        avatarImageView.kf.indicatorType = .activity
        avatarImageView.kf.setImage(with: URL(string: user.picture.thumbnail))
        mainLabel.text = user.name.full
        subtitleLabel.text = user.email
    }
}
