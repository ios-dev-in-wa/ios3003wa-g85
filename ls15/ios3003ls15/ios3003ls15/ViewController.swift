//
//  ViewController.swift
//  ios3003ls15
//
//  Created by WA on 01.06.2021.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    private let activityIndicator = UIActivityIndicatorView(style: .large)
    private let service = RandomUserService()
    private var users = [User]()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupActivityView()
        tableView.dataSource = self
//        tableView.delegate = self
        service.getUsers { [weak self] users in
            print("RESPONSE RESULT")
            self?.users = users
            DispatchQueue.main.async {
                self?.activityIndicator.stopAnimating()
                self?.tableView.reloadData()
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("viewDidAppear")
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetails",
           let destVC = segue.destination as? DetailViewController {
            destVC.user = users[tableView.indexPathForSelectedRow!.row]
        }
    }

    func setupActivityView() {
        view.addSubview(activityIndicator)
        activityIndicator.center = view.center
        activityIndicator.startAnimating()
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "UserTableViewCell", for: indexPath) as? UserTableViewCell else { fatalError() }
        cell.setupWith(users[indexPath.row])
        return cell
    }
}
