//
//  UserResponse.swift
//  ios3003ls15
//
//  Created by WA on 01.06.2021.
//

import Foundation

struct UserResponse: Codable {
    let results: [User]
}
