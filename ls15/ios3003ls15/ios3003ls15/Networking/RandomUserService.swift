//
//  RandomUserService.swift
//  ios3003ls15
//
//  Created by WA on 01.06.2021.
//

import Foundation

class RandomUserService {

    private let session = URLSession.shared

    // https://randomuser.me/api/?results=20

    func getUsers(completion: @escaping (([User]) -> Void)) {
        let request = URLRequest(url: URL(string: "https://randomuser.me/api/?results=20")!)

        session.dataTask(with: request) { data, response, error in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            
            guard let httpResponse = response as? HTTPURLResponse else { return }
            if httpResponse.statusCode == 200,
               let data = data {
//                print(String(data: data, encoding: .utf8))
                do {
                    let userResponse = try JSONDecoder().decode(UserResponse.self, from: data)
                    completion(userResponse.results)
                } catch {
                    print(error.localizedDescription)
                }
            }
        }.resume()
    }
}
