//
//  User.swift
//  ios3003ls15
//
//  Created by WA on 01.06.2021.
//

import Foundation

struct User: Codable {
    let name: FullName
    let email: String
    let picture: Picture
    let phone: String
    let dob: DateOfBirth
    let nat: String
    let gender: String
}

struct FullName: Codable {
    let title: String
    let first: String
    let last: String

    var full: String {
        return "\(title) \(first) \(last)"
    }
}

struct Picture: Codable {
    let large: String
    let medium: String
    let thumbnail: String
}

struct DateOfBirth: Codable {
    let date: String
    let age: Int
}
