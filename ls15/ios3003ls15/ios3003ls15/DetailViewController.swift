//
//  DetailViewController.swift
//  ios3003ls15
//
//  Created by WA on 01.06.2021.
//

import UIKit
import Kingfisher

class DetailViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var dateOfBirthLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!

    var user: User?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imageView.layer.cornerRadius = 120
        setupUI()
    }

    private func setupUI() {
        guard let user = user else { return }
        imageView.kf.setImage(with: URL(string: user.picture.large))
        fullNameLabel.text = user.name.full
        emailLabel.text = user.email
        
        // Standart
//        Date().timeIntervalSince1970
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let dateFormatted = user.dob.date
        let date = formatter.date(from: dateFormatted)
        let newFormatter = DateFormatter()
        newFormatter.dateFormat = "MMM d, yyyy"
        let dateString = newFormatter.string(from: date!)
        dateOfBirthLabel.text = dateString

        phoneLabel.text = user.phone
        
        countryLabel.text = Locale.current.localizedString(forRegionCode: user.nat)
        genderLabel.text = user.gender
    }
}
