//
//  SwiftRobotControlCenter.swift
//  MyRobot
//
//  Created by Ivan Vasilevich on 10/4/14.
//  Copyright (c) 2014 Ivan Besarab. All rights reserved.
//
import UIKit
//  All robot commands can be founded in GameViewController.h
class SwiftRobotControlCenter: RobotControlCenter {
    //  Level name setup
    override func viewDidLoad() {
        levelName = "L4H" //  Level name
        super.viewDidLoad()
    }
    
    override func run() {
        moveFromLeftToRight()
        turnLeft()
        while frontIsClear {
            move()
        }
        turnRight()
        turnRight()
        moveFromLeftToRight()
    }
    
    func moveFromLeftToRight() {
        while rightIsClear {
            if noCandyPresent {
                put()
            }
            move()
            turnRight()
            move()
            turnLeft()
        }
        put()
    }
    func turnLeft() {
        turnRight()
        turnRight()
        turnRight()
    }
}
