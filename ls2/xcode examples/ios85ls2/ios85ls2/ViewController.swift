
//  ViewController.swift
//  ios85ls2
//
//  Created by WA on 01.04.2021.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        foo()
//        letVarExample()
//        stringExample()
        greeting(name: "Artem", times: 2)
        greeting(name: "Ihor", times: 3)
        greeting(name: "Alona", times: 0)

        print(greetingText(name: "Nazar"))

        print(5 % 2)
        // ==
        // !=
        forInExample()
        addBox(x: 10, y: 10)
        addBox(x: 10, y: 150)
    }

    func addBox(x: CGFloat, y: CGFloat) {
        let box = UIView(frame: CGRect(x: x, y: y, width: 50, height: 50))
        box.backgroundColor = .black
        view.addSubview(box)
    }

    func forInExample() {
        for i in 0...20 {
            print(i)
        }
    }

    func foo() {
        // Double
        // Int
        // String
        // Float
        // Bool
//        let divider: Float = 2.3
        let divider = 2.3
        let value = 2
        print(Double.pi)
        // / - delit
        // * - умножает
        // + - добавляет
        // - отнимает
        // % - остаток от деления
        
        // < >
        // <= >=
        // != - проверка неравности
        // == - проверка равности
        
        // Bool
        // true/false
        let result = Double(value) * divider
        print(result)
    }

    func stringExample() {
        let name = "Artem"

        print(name.first, name)

        let surname = "Velykyy"

        let fullName = name + surname
        print(fullName)
    }

    func letVarExample() {
        let maxAttemptsNumber = 10
        var currentAttempt = 0
        
        for _ in 0...20 {
            print(currentAttempt)
            if currentAttempt == 10 {
                currentAttempt += 1
//                print("WOW!!!!")
            } else {
                break
            }
        }
        print(currentAttempt)
    }

    func greeting(name: String, times: Int) {
        if times == 0 {
            return
        }
        for _ in 1...times {
//            print("Hello, \(name)!")
            let text = greetingText(name: name)
            print(text)
        }
    }
//
//    func greetingText(name: String) -> String {
//
//        if name == "Artem" {
//            return "Hey, sir"
//        }
//        let greeting = "Hello, my dear \(name)!"
//        return greeting
//    }

    func greetingText(name: String) -> String {
        switch name {
        case "Artem":
            return "Hey, sir"
        case "Alona":
            return "Hey, miss"
        default:
            return "Hello, my dear \(name)"
        }
    }

    func multiply(valueOne: Int, valueTwo: Int) -> Int {
        return valueOne * valueTwo
    }
}

