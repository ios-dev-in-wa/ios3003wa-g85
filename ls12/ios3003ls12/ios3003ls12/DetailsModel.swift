//
//  DetailsModel.swift
//  ios3003ls12
//
//  Created by WA on 20.05.2021.
//

import Foundation
import Parse

//enum CarsError: Error {
//    case noCars
//}

class DetailsModel {
    // Result<Type, Error>
    // Type - success
    // Error - ошибку
    func readCars(completion: @escaping ((Result<[Car], Error>) -> Void)) {
        let query = PFQuery(className: "Car")

        query.findObjectsInBackground { (objects, error) in
            if let error = error {
                print(error.localizedDescription)
                completion(.failure(error))
                return
            }
            guard let objects = objects else { return }
            var cars = [Car]()
            for object in objects {
                guard let car = self.createCar(object: object) else {
                    return
                }
                cars.append(car)
            }
            completion(.success(cars))
//            delegate?.didLoadCars(cars)
        }
    }

    func createCar(car: Car, completion: @escaping ((Result<Car, Error>) -> Void)) {
        let parseObject = PFObject(className:"Car")

        parseObject["make"] = car.name
        parseObject["productionYear"] = car.productionYear
        parseObject["wasInAccident"] = car.wasInAccident

        parseObject.saveInBackground { (isSuccess, error) in
            if let error = error {
                completion(.failure(error))
                return
            }
            guard let car = self.createCar(object: parseObject) else { return }
            completion(.success(car))
        }
    }

    private func createCar(object: PFObject) -> Car? {
        guard let make = object["make"] as? String,
              let productionYear = object["productionYear"] as? Int,
              let wasInAccident = object["wasInAccident"] as? Bool,
              let objectId = object.objectId else { return nil}
        return Car(name: make,
                   productionYear: productionYear,
                   wasInAccident: wasInAccident,
                   objectId: objectId)
    }
}
