//
//  DetailsViewController.swift
//  ios3003ls12
//
//  Created by WA on 20.05.2021.
//

import UIKit

class DetailsViewController: UIViewController {

    @IBOutlet weak var makeLabel: UILabel!
    @IBOutlet weak var productionYearLabel: UILabel!
    @IBOutlet weak var mainStackView: UIStackView!
    @IBOutlet weak var emptyStateLabel: UILabel!
    @IBOutlet weak var wasInAccidentLAbel: UILabel!
    
    var car: Car?

    override func viewDidLoad() {
        super.viewDidLoad()

        makeLabel.text = car?.name
        if let year = car?.productionYear {
            productionYearLabel.text = "\(year)"
            emptyStateLabel.isHidden = true
            mainStackView.isHidden = false
        }
        wasInAccidentLAbel.text = car?.wasInAccident == true ? "Yes" : "No"
//        traitCollection
        print(UIDevice.current.name, UIDevice.current.model)
        print(UIDevice.current.userInterfaceIdiom)

//        Locale.isoRegionCodes.forEach {
//            print(Locale.current.localizedString(forRegionCode: $0))
//        }
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        print("traitCollectionDidChange")
    }
}
