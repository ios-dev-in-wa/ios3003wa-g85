//
//  MainTableViewCell.swift
//  ios3003ls12
//
//  Created by WA on 20.05.2021.
//

import UIKit

class MainTableViewCell: UITableViewCell {

    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var secondaryLabel: UILabel!
    
}
