//
//  ViewController.swift
//  ios3003ls12
//
//  Created by WA on 20.05.2021.
//

import UIKit

struct Car {
    let name: String
    let productionYear: Int
    let wasInAccident: Bool
    let objectId: String
}

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    let model = DetailsModel()
    let control = UIRefreshControl()

    var cars = [Car]()
//        Car(name: "Mazda", productionYear: 1999),
//        Car(name: "Benz", productionYear: 2020),
//        Car(name: "BMW", productionYear: 2012),
    

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self

        tableView.refreshControl = control
        control.addTarget(self, action: #selector(refreshData), for: .valueChanged)


        let nib = UINib(nibName: "MainTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "MainTableViewCell")

        refreshData()
    }

    @objc func refreshData() {
        model.readCars { result in
            switch result {
            case .success(let cars):
                print(cars)
                self.cars = cars
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.control.endRefreshing()
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToAddCar",
           let vc = segue.destination as? AddCarViewController {
            vc.carCreated = { car in
                self.model.createCar(car: car) { result in
                    switch result {
                    case .success(let car):
                        self.cars.append(car)
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                        }
                    case .failure(let error):
                        print(error.localizedDescription)
                    }
                }
            }
        }
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cars.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MainTableViewCell", for: indexPath) as? MainTableViewCell else {
            fatalError()
        }
        let currentCar = cars[indexPath.row]
        cell.mainLabel.text = currentCar.name
        cell.secondaryLabel.text = "\(currentCar.productionYear) year"
        return cell
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        guard let navC = storyboard.instantiateViewController(withIdentifier: "DetailsNavC") as? UINavigationController,
              let detailsVC = navC.viewControllers.first as? DetailsViewController else { return }
        detailsVC.car = cars[indexPath.row]
        splitViewController?.showDetailViewController(navC, sender: nil)
//        performSegue(withIdentifier: "showDetails", sender: cars[indexPath.row])
    }
}
