//
//  AddCarViewController.swift
//  ios3003ls12
//
//  Created by WA on 20.05.2021.
//

import UIKit

class AddCarViewController: UIViewController {

    @IBOutlet weak var makeTextField: UITextField!
    @IBOutlet weak var productionYearTextfield: UITextField!
    @IBOutlet weak var wasInAccidentSwitch: UISwitch!

    var carCreated: ((Car) -> Void)?

    @IBAction func saveAction(_ sender: UIButton) {
        guard let make = makeTextField.text,
              let year = productionYearTextfield.text else { return }
        carCreated?(Car(name: make,
                        productionYear: Int(year) ?? 2000,
                        wasInAccident: wasInAccidentSwitch.isOn,
                        objectId: ""))
        dismiss(animated: true, completion: nil)
    }
}
